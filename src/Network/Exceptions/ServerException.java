package Network.Exceptions;

public class ServerException extends Exception{
    public ServerException(String text){
        super(text);
    }
}
