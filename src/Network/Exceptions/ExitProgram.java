package Network.Exceptions;

public class ExitProgram extends Exception{
    public ExitProgram(String text){
        super(text);
    }
}
