package Network.Server.Model;

import Network.Controller.ClientHandler;
import Network.Server.View.ServerView;
import UNO.Model.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Objects;

public class Server implements Runnable {

    public ArrayList<ClientHandler> clientHandlers;
    public int ready;
    private Game game;
    private ServerSocket serverSocket;
    private int numberOfPlayers;
    private ServerView view;
    private ArrayList<Player> players = new ArrayList<>(2);
    private Player currentPlayer;
    private Player otherPlayer;

    public Server() {
        clientHandlers = new ArrayList<>();
        view = new ServerView();
        numberOfPlayers = 1;
        ready = 0;
    }

    public static void main(String[] args) {
        Server server = new Server();
        System.out.println("Starting up...");
        server.run();
    }

    @Override
    public void run() {
        boolean openNewSocket = true;
        while (openNewSocket) {
            try {
                serverSetUp();
                while (true) {
                    Socket socket = serverSocket.accept();
                    String name = "Player " + String.format("%02d", numberOfPlayers++);
                    view.showMessage(name + " has connected!");
                    ClientHandler handler = new ClientHandler(socket, this);
                    new Thread(handler).start();
                    clientHandlers.add(handler);
                }
            } catch (IOException e) {
                System.out.println("Oops, Wrong input..." + e.getMessage());
                if (view.getBoolean("Do you want to open a new socket?")) {
                    openNewSocket = false;
                }
            }
        }
    }

    /**
     * Has the task of letting a user know that a connection has been successfully established
     *
     * @param playerName is a String representing the name of the player that has just connected
     */
    public String response(String playerName, String playerType) {
        view.showMessage("Responding to the handshake...");
        String niceType = "You will play as a ";
        if(Objects.equals(playerType, "H"))
            niceType += "human player.";
        else
            niceType += "computer player.";
        return "Welcome to UNO " + playerName + ". " + niceType + " Type READY to get started." + "\n" + Commands.END;
    }

    /**
     * Sets up a Game using the players list
     */
    public synchronized void gameSetUp() {
        game = new Game(players);
        this.currentPlayer = game.getPlayerList().get(game.getCurrentPlayerIndex());
        if (game.getCurrentPlayerIndex() == 1)
            this.otherPlayer = game.getPlayerList().get(0);
        else this.otherPlayer = game.getPlayerList().get(1);
    }

    /**
     * Sends a message to both clients
     *
     * @param message is a String that represents the message that should be sent
     */
    public synchronized void sendToBoth(String message) {
        for (ClientHandler handler : clientHandlers) {
            handler.messageForClient(message);
        }
    }

    /**
     * Sends a message to one specific client
     *
     * @param name    is a String representing the name of the client for which the message is intended
     * @param message is a String representing the message that should be sent
     */
    public void sendToOne(String name, String message) {
        for (ClientHandler ch : clientHandlers) {
            if (ch.getName().equals(name)) {
                ch.messageForClient(message);
            }
        }
    }

    /**
     * Will send the cards to the specific player
     *
     * @param player the player from which we intend to get the cards from
     * @return the cards that are in the hand of the specific player
     */
    public ArrayList<Card> sendCards(Player player) {
        return game.getPlayerHand(player);
    }

    /**
     * Has the task of informing a certain player about their turn and deck
     *
     * @param player is the current player that has to make a move
     * @param cards  is a List of Card Objects representing the player's deck
     * @return a String representing all the information valuable for a player in order to make a move
     */
    public String notifyPlayerText(Player player, ArrayList<Card> cards) {
        return "Current player: " + player.getName() + "\nCards: " + cards;
    }

    /**
     * @param cards the player's new hand
     * @return a String with the new cards of the player
     */
    public String newCardsNotify(ArrayList<Card> cards) {
        return "Your new cards are: " + cards;
    }

    /**
     * Sends valuable information to the players once they are ready to play UNO
     */
    public synchronized boolean readyToPlay() {
        if (ready == 2) {
            view.showMessage("Let's play a game of UNO!");
            gameSetUp();

            sendToBoth("The following commands can be used: " + "\n" + "- QUIT: You quit and the game is going to end!" + "\n" +
                    "- MOVE: You select the index (starting from 1) of the card that you want to place and the color, in case the card is of WILD type " +
                    "such as: MOVE 1blue or MOVE 3" +
                    "\n" + "- DRAW: You can draw a card from the deck!\n" +
                    "- CHAT: You will send a message to the other player, make sure to type the message after the command.\n" + Commands.END);

            sendToBoth("We are ready to play UNO!" + "\n" + Commands.END);
            sendToBoth("The first card is: " + game.getCardsDown().get(0) + "\n" + Commands.END);
            sendToOne(currentPlayer.getName(), notifyPlayerText(currentPlayer, game.getPlayerHand(currentPlayer)) + "\n" + Commands.END);
            sendToOne(otherPlayer.getName(), notifyPlayerText(otherPlayer, game.getPlayerHand(otherPlayer))  + "\n" + Commands.END);

            //the special cases when a DrawTwo, Reverse, Skip, Wild, WildDrawFour(cannot be played) card comes in as the first card
            if (game.getCardsDown().get(0).getValue().equals(Card.Value.DrawTwo)) {
                game.getPlayerHand(game.getCurrentPlayer()).add(game.getDeck().drawCard());
                game.getPlayerHand(game.getCurrentPlayer()).add(game.getDeck().drawCard());
                sendToOne(currentPlayer.getName(), newCardsNotify(game.getPlayerHand(currentPlayer)) + "\n" + Commands.END);
                sendToOne(currentPlayer.getName(), "Your turn has been skipped and you received 2 cards!" + "\n" + Commands.END);
                game.nextPlayer();
            } else if (game.getCardsDown().get(0).getValue().equals(Card.Value.Reverse)) {
                sendToOne(currentPlayer.getName(), "Your turn has been skipped and the game direction changed!" + "\n" + Commands.END);
                game.nextPlayer();
            } else if (game.getCardsDown().get(0).getValue().equals(Card.Value.Skip)) {
                sendToOne(currentPlayer.getName(), "Your turn has been skipped!" + "\n" + Commands.END);
                game.nextPlayer();
            } else if (game.getCardsDown().get(0).getValue().equals(Card.Value.Wild)) {
                String color = view.getString("What color do you pick? (green, blue, red, yellow)");
                game.changeCurrentColor(color);
                sendToBoth("The current color is: " + color + ".\n" + Commands.END);
            } else {
                sendToOne(otherPlayer.getName(), "Please wait " + currentPlayer.getName() + " is playing.\n" + Commands.END);
            }
            return true;
        } else {
            view.showMessage("Awaiting players...");
            return false;
        }
    }

    /**
     * Sets up a server on a specified port
     */
    public synchronized void serverSetUp() {
        serverSocket = null;
        while (serverSocket == null) {
            int port = view.getInt("Please enter the port of the server: ");
            try {
                view.showMessage("Trying to open a socket at 127.0.0.1 on port " + port + "...");
                serverSocket = new ServerSocket(port);
                view.showMessage("The server has started on port " + port);
            } catch (IOException e) {
                view.showMessage("Oops... unable to create a socket on 127.0.0.1 and port " + port + ".");
                if (view.getBoolean("Would you like to try again?")) {
                    System.out.println("User gave up...");
                }
            }
        }
    }

    /**
     * Has the task of signaling any error of input
     *
     * @param error is a String representing the error name
     */
    public synchronized String notifyError(String error) {
        view.showMessage("Sending the error...");
        return Commands.ERROR + Commands.SEPARATOR + error + "\n" + Commands.END;
    }

    /**
     * Increases the value assigned to the parameter ready by one
     */
    public synchronized void incrementReady() {
        this.ready++;
    }

    /**
     * Informs the players that the game has come to an end and also notifies them about the result
     *
     * @param endType the type of the command used to end the game
     */
    public synchronized String gameOver(String endType) {
        return Commands.GAMEOVER + Commands.SEPARATOR + endType + Commands.SEPARATOR + players.get(0).getName()
                + Commands.SEPARATOR + game.getPlayerScores(players.get(0)) + Commands.SEPARATOR + players.get(1).getName() + Commands.SEPARATOR + game.getPlayerScores(players.get(1)) + "\n" + Commands.END;
    }

    /**
     * Ends the game in a controlled manner
     */
    public String endGame() {
        return Commands.GAMEOVER;
    }

    /**
     * @return the server view
     */
    public ServerView getView() {
        return view;
    }

    /**
     * @return the current game of UNO
     */
    public Game getGame() {
        return game;
    }

    /**
     * @return the list of players
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }
}
