package Network.Server.View;

import java.io.PrintWriter;
import java.util.Scanner;

public class ServerView {
    private PrintWriter writer;

    public ServerView() {
        writer = new PrintWriter(System.out, true);
    }

    /**
     * Prints a String to the console and checks for user input
     *
     * @param text is a String representing the message that should be shown to the user
     * @return the input of the user (a Boolean value)
     */
    public boolean getBoolean(String text) {
        Scanner s = new Scanner(System.in);
        System.out.println(text);
        System.out.println("Type only 'yes' or 'no', please!");
        String in = s.nextLine();
        return !in.equals("yes");
    }

    /**
     * Prints a String to the console and checks for user input
     *
     * @param text is a String representing the message that should be shown to the user
     * @return the input of the user (an Integer value)
     */
    public int getInt(String text) {
        Scanner s = new Scanner(System.in);
        System.out.println(text);
        String in = s.nextLine();
        return Integer.parseInt(in);
    }

    /**
     * Prints a String to the console and checks for user input
     *
     * @param text is a String representing the message that should be shown to the user
     * @return the input of the user (a String value)
     */
    public String getString(String text){
        Scanner s = new Scanner(System.in);
        System.out.println(text);
        return s.nextLine();
    }

    /**
     * Prints a message to the console
     *
     * @param message a String representing the said message
     */
    public void showMessage(String message) {
        writer.println(message);
    }
}
