package Network.Controller;

import Network.Server.Model.Server;
import Network.Exceptions.GameException;
import UNO.Model.Card;
import UNO.Model.Commands;
import UNO.Model.ComputerPlayer;
import UNO.Model.Player;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;

public class ClientHandler implements Runnable {

    private Socket socket;
    private BufferedReader readFromClient;
    private BufferedWriter writeToClient;
    private Server server;
    private String name;

    public ClientHandler(Socket socket, Server server) {
        try {
            readFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writeToClient = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.socket = socket;
            this.server = server;
        } catch (IOException e) {
            server.getView().showMessage("Oops... Unable to handle client.");
            kill();
        }
    }

    public void run() {
        String message;
        try {
            while (socket.isConnected()) {
                if (server.getGame() != null && server.getGame().getCurrentPlayer() instanceof ComputerPlayer) {
                    if (((ComputerPlayer) server.getGame().getCurrentPlayer()).indexPlayableCard(server.getGame(), server.getGame().getPlayerHand(server.getGame().getCurrentPlayer())) == -1) {
                        String draw = Commands.DRAW;
                        server.getView().showMessage(draw);
                        inputHandler(draw);
                    } else {
                        String completeMove = Commands.MOVE + " " + ((ComputerPlayer) server.getGame().getCurrentPlayer()).indexPlayableCard(server.getGame(), server.getGame().getPlayerHand(server.getGame().getCurrentPlayer()));
                        if (((ComputerPlayer) server.getGame().getCurrentPlayer()).getColor() != null)
                            completeMove += ((ComputerPlayer) server.getGame().getCurrentPlayer()).getColor();
                        server.getView().showMessage(completeMove);
                        inputHandler(completeMove);
                        // check if the previous message is new line...stop afterwards...didnt work :(
                    }
                } else {
                    message = readFromClient.readLine();
                    inputHandler(message);
                }
            }
        } catch (IOException | GameException i) {
            server.getView().showMessage("Client handler exception");
            kill();
        }
    }

    /**
     * Writes a message for the client to see
     *
     * @param text is a String representing the said message
     */
    public synchronized void messageForClient(String text) {
        try {
            writeToClient.write(text);
            writeToClient.newLine();
            writeToClient.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Handles every command that the client writes and executes it
     *
     * @param input the input from the client
     * @throws GameException
     * @throws IOException
     */
    public void inputHandler(String input) throws GameException, IOException {
        String[] splitInput = input.split(Commands.SEPARATOR);
        String command = splitInput[0];
        String name = "";
        switch (command) {
            case Commands.HELLO -> {
                if (splitInput.length == 1) {
                    messageForClient("You are not allowed to write an empty HELLO command." + "\n" + Commands.END);
                } else {
                    name = splitInput[1];
                    this.name = name;
                    String playerType = "C";
                    if (splitInput.length != 3) {
                        server.getPlayers().add(new ComputerPlayer(name));
                    } else {
                        playerType = splitInput[2];
                        if (playerType.equals("H")) {
                            server.getPlayers().add(new Player(name));
                        } else {
                            server.getPlayers().add(new ComputerPlayer(name));
                        }
                    }
                    messageForClient(server.response(name, playerType));
                }
            }
            case Commands.READY -> {
                server.incrementReady();
                if (!server.readyToPlay()) {
                    messageForClient("Waiting for players..." + "\n" + Commands.END);
                }
            }
            case Commands.DRAW -> {
                Card drawnCard = server.getGame().getDeck().drawCard();
                server.getGame().getPlayerHand(server.getGame().getCurrentPlayer()).add(drawnCard);
                if (server.getGame().isValid(server.getGame().getCardsDown().get(server.getGame().getCardsDown().size() - 1), drawnCard)) {
                    //check when the drawn card is a wild or wildFour card (give a random color) && skip && reverse
                    server.getGame().placeCard(server.getGame().getCurrentPlayer(), drawnCard);
                    messageForClient("The drawn card has been played for you!");
                    server.sendToBoth("The top card is: " + server.getGame().getCardsDown().get(server.getGame().getCardsDown().size() - 1) + "\n" + Commands.END);
                }
                server.sendToOne(server.getGame().getCurrentPlayer().getName(),
                        server.notifyPlayerText(server.getGame().getCurrentPlayer(), server.sendCards(server.getGame().getCurrentPlayer())) + "\n" + Commands.END);
                server.getGame().nextPlayer();
                // for the next player
                server.sendToOne(server.getGame().getCurrentPlayer().getName(), "It's your turn now!" + "\n" + Commands.END);
                messageForClient("It's the other player's turn." + "\n" + Commands.END);
            }
            case Commands.MOVE -> {
                String s1 = splitInput[1];
                String s2 = splitInput[1];
                int indexOfCard = Integer.parseInt(s1.replaceAll("[^0-9]+", "")) - 1;
                String color = s2.replaceAll("[^A-Za-z]", "");
                boolean isSevenOuno = false; //to change the hands and the card must be played from the initial hand
                boolean isSkipTurn = false;  //to skip the turn when playing DrawTwo, Skip, WildDrawFour and notifyAfterPlace will get problems
                boolean isDrawCard = false;  //to send the new deck after you receive the cards (from the draw cards - DrawTwo & WildDrawFour)
                ArrayList<Card> hand = server.getGame().getPlayerHand(server.getGame().getCurrentPlayer());
                if (indexOfCard > hand.size()) {
                    messageForClient("The index you have picked is out of range.");
                } else {
                    Card cardToBePlayed = hand.get(indexOfCard);
                    if (server.getGame().isValid(server.getGame().getCardsDown().get(server.getGame().getCardsDown().size() - 1), cardToBePlayed)) {
                        if (server.getGame().isWildCard(cardToBePlayed)) {
                            if (cardToBePlayed.getValue().equals(Card.Value.Wild)) {
                                // changes the current color of the game
                                if (color.length() == 0) {
                                    messageForClient("You did not pick the color" + "\n" + Commands.END);
                                } else {
                                    server.getGame().changeCurrentColor(color);
                                    server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                                    server.sendToBoth("The current color is: " + server.getGame().getCurrentColor() + "\n" + Commands.END);
                                }
                            } else if (cardToBePlayed.getValue().equals(Card.Value.WildFour)) {
                                // next player draws 4 cards and loses their turn
                                ArrayList<Card> cardsCurrPlayer = server.getGame().getPlayerHand(server.getGame().getCurrentPlayer());
                                isSkipTurn = true;
                                isDrawCard = true;
                                for (Card card : cardsCurrPlayer) {
                                    if (card.getColor() == server.getGame().getCurrentColor() || card.getValue().equals(server.getGame().getCardsDown().get(server.getGame().getCardsDown().size() - 1).getValue()))
                                        messageForClient("You are not allowed to play this card because you have a card" +
                                                " in your hand that matches the color of the card that is currently down." + "\n" + Commands.END);
                                }
                                if (color.length() == 0) {
                                    messageForClient("You did not pick the color" + "\n" + Commands.END);
                                } else {
                                    server.getGame().changeCurrentColor(color);
                                    server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                                    server.sendToBoth("The current color is: " + server.getGame().getCurrentColor() + "\n" + Commands.END);
                                    for (Player p : server.getGame().getPlayerList()) {
                                        if (!server.getGame().getCurrentPlayer().equals(p)) {
                                            server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                            server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                            server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                            server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                        }
                                    }
                                }
                            }
                        } else if (server.getGame().isActionCard(cardToBePlayed)) {
                            if (cardToBePlayed.getValue().equals(Card.Value.DrawTwo)) {
                                //forward the 2 cards to the next player
                                isSkipTurn = true;
                                isDrawCard = true;
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                                for (Player p : server.getGame().getPlayerList()) {
                                    if (!server.getGame().getCurrentPlayer().equals(p)) {
                                        server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                        server.getGame().getPlayerHand(p).add(server.getGame().getDeck().drawCard());
                                    }
                                }
                                //next player draws 2 cards and loses their turn
                            } else if (cardToBePlayed.getValue().equals(Card.Value.Reverse)) {
                                //reverse the direction of play
                                isSkipTurn = true;
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                                server.getGame().changeDirectionOfPlay();
                            } else if (cardToBePlayed.getValue().equals(Card.Value.Skip)) {
                                //next player loses their turn
                                isSkipTurn = true;
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                                server.getGame().changeDirectionOfPlay();
                            }
                        }//Seven-O UNO non-mandatory part
                        else if (server.getGame().isNumberCard(cardToBePlayed)) {
                            if (cardToBePlayed.getValue().equals(Card.Value.Zero)) {
                                //every time this card is played all players pass their hands to the next player in direction of play
                                isSevenOuno = true;
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                            } else if (cardToBePlayed.getValue().equals(Card.Value.Seven)) {
                                //the player who played this card MUST trade their hand with a player of their choice
                                isSevenOuno = true;
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                            } else {
                                server.getGame().placeCard(server.getGame().getCurrentPlayer(), cardToBePlayed);
                            }
                        }
                    } else {
                        messageForClient("You are not allowed to place this card!");
                    }
                }
                if (server.getGame().isRoundOver(server.getGame().getCurrentPlayer())) {
                    Player p0 = server.getGame().getPlayerList().get(0);
                    Player p1 = server.getGame().getPlayerList().get(1);
                    int prevScoreP0 = server.getGame().getPlayerScores(p0);
                    int prevScoreP1 = server.getGame().getPlayerScores(p1);
                    int scoreP0 = server.getGame().calculateScore(server.getGame().getPlayerHand(p1));
                    int scoreP1 = server.getGame().calculateScore(server.getGame().getPlayerHand(p0));
                    int totalScoreP0 = scoreP0 + prevScoreP0;
                    int totalScoreP1 = scoreP1 + prevScoreP1;
                    int winnerScore;
                    if (totalScoreP0 >= 500 || totalScoreP1 >= 500) {
                        //game over & declare winner
                        String winner = p1.getName();
                        winnerScore = totalScoreP1;
                        if (totalScoreP0 > totalScoreP1) {
                            winner = p0.getName();
                            winnerScore = totalScoreP0;
                        }
                        server.getGame().setScore(p0, totalScoreP0);
                        server.getGame().setScore(p1, totalScoreP1);
                        server.getGame().showScores();
                        server.sendToBoth("The game is over! Congratulations, " + winner + " has won with " + winnerScore + " points!" + "\n" + Commands.END);
                        server.endGame();
                        server.sendToBoth(server.gameOver(Commands.END));
                        server.clientHandlers.get(0).kill();
                        server.clientHandlers.get(1).kill();
                        System.exit(0);
                    } else {
                        //round over & show scores
                        server.getGame().setScore(p0, totalScoreP0);
                        server.getGame().setScore(p1, totalScoreP1);
                        server.getGame().showScores();

                        server.sendToOne(p0.getName(), "Your score after this round is: " + totalScoreP0 + "." + "\n" + Commands.END);
                        server.sendToOne(p1.getName(), "Your score after this round is: " + totalScoreP1 + "." + "\n" + Commands.END);
                        server.getGame().startNewRound();
                        server.sendToBoth("The first card is " + server.getGame().getCardsDown().get(0) + "\n" + Commands.END);
                        for (int i = 0; i < 2; i++) {
                            if (!server.getGame().getCurrentPlayer().equals(server.getGame().getPlayerList().get(i))) {
                                server.sendToOne(server.getGame().getPlayerList().get(i).getName(), server.getGame().getPlayerHand(server.getGame().getPlayerList().get(i)) + "\n" + Commands.END);
                                server.sendToOne(server.getGame().getPlayerList().get(i).getName(), server.getGame().getPlayerHand(server.getGame().getPlayerList().get(i)).toString() + "\n" + Commands.END);
                                server.sendToOne(server.getGame().getPlayerList().get(i).getName(), " please wait " + server.getGame().getCurrentPlayer().getName() + " is playing" + "\n" + Commands.END);
                            }
                        }
                    }
                } else {
                    //round is not over
                    server.sendToBoth("The top card is: " + server.getGame().getCardsDown().get(server.getGame().getCardsDown().size() - 1) + "\n" + Commands.END);
                    if (!isSevenOuno)
                        server.sendToOne(server.getGame().getCurrentPlayer().getName(),
                                server.notifyPlayerText(server.getGame().getCurrentPlayer(), server.sendCards(server.getGame().getCurrentPlayer())) + "\n" + Commands.END);
                    if (isSkipTurn) {
                        if (isDrawCard) {
                            for (int i = 0; i < 2; i++)
                                if (!server.getGame().getCurrentPlayer().equals(server.getGame().getPlayerList().get(i)))
                                    server.sendToOne(server.getGame().getPlayerList().get(i).getName(), server.newCardsNotify(server.getGame().getPlayerHand(server.getGame().getPlayerList().get(i))) + "\n" + Commands.END);
                        }
                        server.getGame().nextPlayer();
                        //for the next player
                        server.sendToOne(server.getGame().getCurrentPlayer().getName(), "Your turn has been skipped!" + "\n" + Commands.END);
                        server.getGame().nextPlayer();
                    } else {
                        if (isSevenOuno) {
                            server.getGame().switchHands();
                            server.sendToOne(server.getGame().getCurrentPlayer().getName(), "The decks have been changed!" + "\n" + Commands.END);
                            server.sendToOne(server.getGame().getCurrentPlayer().getName(), server.newCardsNotify(server.getGame().getPlayerHand(server.getGame().getCurrentPlayer())) + "\n" + Commands.END);
                        }
                        messageForClient("It's the other player's turn." + "\n" + Commands.END);
                        server.getGame().nextPlayer();
                        //for the next player
                        server.sendToOne(server.getGame().getCurrentPlayer().getName(),
                                server.notifyPlayerText(server.getGame().getCurrentPlayer(), server.sendCards(server.getGame().getCurrentPlayer())) + "\n" + Commands.END);
                        server.sendToOne(server.getGame().getCurrentPlayer().getName(), "It's your turn now!" + "\n" + Commands.END);
                    }
                }
            }
            case Commands.QUIT -> {
                server.sendToBoth(server.gameOver(Commands.END));
                server.endGame();
                server.clientHandlers.get(0).kill();
                server.clientHandlers.get(1).kill();
                System.exit(0); //stops the server
            }
            case Commands.CHAT -> {
                String message = input.substring(input.indexOf(" ") + 1);
                System.out.println(message);
                if (server.getGame().getPlayerList().get(0).getName().equals(name)) {
                    server.sendToOne(server.getGame().getPlayerList().get(1).getName(), name + " : " + message + "\n" + Commands.END);
                } else {
                    server.sendToOne(server.getGame().getPlayerList().get(0).getName(), name + " : " + message + "\n" + Commands.END);
                }
            }
            default -> {
                messageForClient(server.notifyError(Commands.ERROR));
                messageForClient("Invalid general command.");
            }
        }
    }

    /**
     * @return the name of the client
     */
    public String getName() {
        return name;
    }

    /**
     * Stops the game and disconnects the player
     */
    public void kill() {
        try {
            writeToClient.flush();
            socket.close();
            server.getView().showMessage("The game is about to stop...");
            // writeToClient.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
