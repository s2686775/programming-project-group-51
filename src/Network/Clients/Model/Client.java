package Network.Clients.Model;

import Network.Clients.View.ClientView;
import Network.Exceptions.ExitProgram;
import Network.Exceptions.GameException;
import Network.Exceptions.ServerException;
import UNO.Model.Commands;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket serverSocket;
    private BufferedReader reader;
    private BufferedWriter writer;
    private ClientView view;
    private boolean turn = true;

    public Client() {
        this.view = new ClientView(this);
    }

    public static void main(String[] args) {
        (new Client()).start();
    }

    /**
    * @return the turn of the player
     **/
    public boolean isTurn() {
        return turn;
    }

    public void start() {
        boolean retry = true;
        while (retry) {
            try {
                createConnection();
                handshake(getText("What is your name? Type 'HELLO name H/C' (add H, if you want to play or C, if you want a computer to play for you)"));
                readInput();
                view.listen();
                retry = false;
            } catch (ServerException | ExitProgram e) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Would you like to try again ?");
                String in = sc.nextLine();
                retry = in.equalsIgnoreCase("yes");
            } catch (IOException | GameException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Connects the client to the server on the localHost on a port chosen by the client
     */
    public void createConnection() throws ExitProgram {
        reset();
        while (serverSocket == null) {
            String host = "127.0.0.1";
            int port = returnInteger("Welcome to UNO!" + "\n" + "Introduce the port on which you would like to be connected:");
            try {
                InetAddress address = InetAddress.getByName(host);
                System.out.println("Connecting to " + address + ":" + port + "...");
                serverSocket = new Socket(address, port);
                reader = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));
                writer = new BufferedWriter(new OutputStreamWriter(serverSocket.getOutputStream()));
            } catch (IOException e) {
                System.out.println("Oops... I was not able to create a socket on " + host + " and " + port + ".");
            }
        }
        System.out.println("Success! You are now connected!");
    }

    /**
     * The handshake is started by the player joining a certain server
     * The server receives this client's name, and replies by welcoming the client to a game of UNO
     */
    public void handshake(String playerName) throws ServerException {
        try {
            sendToServer(playerName);
        } catch (ServerException e) {
            throw new ServerException("Handshake exception");
        }
    }

    /**
     * Reads the input received from the server
     **/
    public void readInput() {
        new Thread(() -> {
            String text;
            while (serverSocket.isConnected()) {
                text = readFromServer();
                System.out.println(text);
            }
        }).start();
    }

    /**
     * Sends a message to the server
     *
     * @param message the message to be sent
     */
    public synchronized void sendToServer(String message) throws ServerException {
        if (writer != null) {
            try {
                writer.write(message);
                writer.newLine();
                writer.flush();
            } catch (IOException e) {
                throw new ServerException("Could not send message to server");
            }
        } else {
            throw new ServerException("Could not send message to server");
        }
    }

    /**
     * Reads from server and constructs a String with what was read
     *
     * @return a String representing what was read from the server
     */
    public String readFromServer() {
        if (reader != null) {
            try {
                StringBuilder builder = new StringBuilder();
                for (String line = reader.readLine(); line != null && !line.equals(Commands.END); line = reader.readLine()) {
                    builder.append(line).append(System.lineSeparator());
                }
                return builder.toString();
            } catch (IOException e) {
                return "Oops... Unable to read from server";
            }
        } else {
            return "Oops... Unable to read from server";
        }
    }


    /**
     * @param question is a String representing the message that will be seen on screen
     * @return the message received by the BufferedReader
     */
    public String getText(String question) {
        System.out.println(question);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        try {
            input = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    /**
     * @param question is a String representing the message that will be shown on the screen
     * @return a number received by the BufferedReader
     */
    public int returnInteger(String question) {
        System.out.println(question);
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        int num = 0;
        try {
            num = Integer.parseInt(in.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return num;
    }

    /**
     * Resets the socket, reader and writer back to null
     */
    public void reset() {
        serverSocket = null;
        reader = null;
        writer = null;
    }

    /**
     * Disconnects the client from the server by closing the reader, the writer and the socket
     */
    public void terminate() {
        try {
            reader.close();
            writer.close();
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * A way for the client to let the server know that they are ready to play
     * For a UNO game to start, there needs to be 2 players that are ready
     */

    public void clientReady() throws ServerException {
        try {
            sendToServer(Commands.READY);
        } catch (ServerException e) {
            throw new ServerException("Client 'Ready' command exception.");
        }
    }

    /**
     * A player types in a message that they want to send to the other player
     * The client sends this message to the server in order for it to be seen by the other player
     */
    public void clientChat(String text) throws ServerException {
        try {
            sendToServer(Commands.CHAT + Commands.SEPARATOR + text);
        } catch (ServerException e) {
            throw new ServerException("Client 'Chat' command exception.");
        }
    }

    /**
     *  Sends to the server the command 'DRAW'
     **/
    public void clientDraw() throws ServerException {
        try {
            sendToServer(Commands.DRAW);
        } catch (ServerException e) {
            throw new ServerException("Client 'Draw' command exception.");
        }
    }

    /**
     * Sends to the server the 'MOVE' command together with the index of the card and the color, when necessary
     *
     * @param command consists of the 'MOVE' command as well as the index of the card and the color, when necessary
     **/
    public void clientMove(String command) throws ServerException {
        try {
            sendToServer(Commands.MOVE + Commands.SEPARATOR + command);
        } catch (ServerException e) {
            throw new ServerException("Client 'Move' command exception.");
        }
    }

    /**
     * A player types in the command 'QUIT' if they want to leave the game
     * The client then sends this information to the server in order to disconnect the player and end the game
     */
    public void clientQuit() throws ServerException {
        try {
            sendToServer(Commands.QUIT);
        } catch (ServerException e) {
            throw new ServerException("Client 'Quit' command exception.");
        }
    }
}
