package Network.Clients.View;

import Network.Clients.Model.Client;
import UNO.Model.Commands;
import Network.Exceptions.GameException;
import Network.Exceptions.ServerException;

import java.io.IOException;
import java.util.Scanner;

public class ClientView {
    private Client client;
    private boolean exitGame = false;

    public ClientView(Client client) {
        this.client = client;
    }

    /**
     * Permanently ready to receive input from the user
     * Calls the inputHandlerView() to deal with the said input
     */
    public void listen() throws ServerException, IOException, GameException {
        Scanner scanner = new Scanner(System.in);
        while (!(exitGame)) {
            while (scanner.hasNext()) {
                String line = scanner.nextLine();
                inputHandlerView(line);
            }
        }
        client.terminate();
    }

    /**
     * Checks to see if the client's input is an actual command or not
     *
     * @param input is a String representing the command specified by the player
     * @return true if the input is an actual command, false otherwise
     */
    public boolean isValid(String input) {
        return switch (input) {
            case Commands.HELLO, Commands.READY, Commands.MOVE, Commands.DRAW, Commands.QUIT, Commands.CHAT -> true;
            default -> false;
        };
    }

    /**
     * Based on the received input, this method decides which of the Client class methods to call
     *
     * @param input is a String representing the command sent by the client
     */
    public void inputHandlerView(String input) throws ServerException {
        String[] inputs = input.split(Commands.SEPARATOR);
        String command = inputs[0];
        if (isValid(command) && client.isTurn()) {
            switch (command) {
                case Commands.READY -> client.clientReady();
                case Commands.MOVE -> client.clientMove(inputs[1]);
                case Commands.DRAW -> client.clientDraw();
                case Commands.QUIT -> {
                    //setExitGame();
                    client.clientQuit();
                    System.exit(0);
                }
                case Commands.CHAT -> {
                    String message = input.substring(input.indexOf(" ") + 1);
                    client.clientChat(message);
                }
            }
        } else {
            System.out.println("Invalid command.");
        }
    }
}
