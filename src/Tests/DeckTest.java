package Tests;

import UNO.Model.Card;
import UNO.Model.Deck;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class DeckTest {
    @Test
    public void testDeckSize() {
        Deck deck = new Deck();
        deck.init();
        assertEquals(108, deck.getSizeDeck());
    }

    @Test
    public void testDrawCard() {
        Deck deck = new Deck();
        deck.init();
        Card card = deck.drawCard();
        assertEquals(107, deck.getSizeDeck());
        assertNotNull(card);
    }

    @Test
    public void testDrawMultipleCards() {
        Deck deck = new Deck();
        deck.init();
        ArrayList<Card> cards = deck.drawMultipleCards(5);
        assertEquals(103, deck.getSizeDeck());
        assertEquals(5, cards.size());
    }

    @Test
    public void testShuffleDeck() {
        Deck deck1 = new Deck();
        deck1.init();
        Deck deck2 = new Deck();
        deck2.init();
        deck1.shuffleDeck();
        assertNotEquals(deck2.getCard(0), deck1.getCard(0));
    }

    @Test
    public void testReplaceCardsInDeck() {
        Deck deck = new Deck();
        deck.init();
        ArrayList<Card> newCards = new ArrayList<>();
        newCards.add(new Card(Card.Color.Red, Card.Value.Seven));
        newCards.add(new Card(Card.Color.Yellow, Card.Value.Nine));
        deck.replaceCardsInDeck(newCards);
        assertEquals(110, deck.getSizeDeck());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDrawMultipleCardsFromEmptyDeck() {
        Deck deck = new Deck();
        deck.init();
        for (int i = 0; i < 108; i++) {
            deck.drawCard();
        }
        deck.drawMultipleCards(5);
    }
}