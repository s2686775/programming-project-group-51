package Tests;

import UNO.Model.Card;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {
    @Test
    public void testCardColor() {
        Card card = new Card(Card.Color.Red, Card.Value.Seven);
        assertEquals(Card.Color.Red, card.getColor());
    }

    @Test
    public void testCardValue() {
        Card card = new Card(Card.Color.Green, Card.Value.Three);
        assertEquals(Card.Value.Three, card.getValue());
    }

    @Test
    public void testCardColors() {
        assertEquals(5, Card.colors.length);
    }

    @Test
    public void testCardToString() {
        Card card = new Card(Card.Color.Yellow, Card.Value.Skip);
        assertEquals("Card{color=Yellow, value=Skip}\n", card.toString());
    }
}