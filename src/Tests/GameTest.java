package Tests;

import UNO.Model.Card;
import UNO.Model.Game;
import UNO.Model.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class GameTest {
    Game game;
    Player player1;
    Player player2;
    ArrayList<Player> players = new ArrayList<>();

    @BeforeEach
    void setUp() {
        Player player1 = new Player("Victor");
        Player player2 = new Player("Iosif");
        players.add(player1);
        players.add(player2);
        game = new Game(players);
    }

    @Test
    void testIsValid() {
        Card card1 = new Card(Card.Color.Blue, Card.Value.Eight);
        Card card2 = new Card(Card.Color.Red, Card.Value.DrawTwo);
        Card card3 = new Card(Card.Color.Yellow, Card.Value.Nine);
        Card card4 = new Card(Card.Color.Blue, Card.Value.Nine);
        Assertions.assertFalse(game.isValid(card1, card2));
        Assertions.assertTrue(game.isValid(card3, card4));
    }

    @Test
    void testCalculateScore() {
        Map<Player, ArrayList<Card>> testPlayer1Score = new HashMap<>();
        Map<Player, ArrayList<Card>> testPlayer2Score = new HashMap<>();
        ArrayList<Card> cardsPlayer1 = new ArrayList<>();
        ArrayList<Card> cardsPlayer2 = new ArrayList<>();
        Card card1 = new Card(Card.Color.Blue, Card.Value.Eight);
        Card card2 = new Card(Card.Color.Red, Card.Value.DrawTwo);
        Card card3 = new Card(Card.Color.None, Card.Value.WildFour);
        cardsPlayer1.add(card1);
        cardsPlayer1.add(card2);
        cardsPlayer1.add(card3);
        Card card4 = new Card(Card.Color.Yellow, Card.Value.Nine);
        Card card5 = new Card(Card.Color.Red, Card.Value.Wild);
        Card card6 = new Card(Card.Color.None, Card.Value.WildFour);
        cardsPlayer2.add(card4);
        cardsPlayer2.add(card5);
        cardsPlayer2.add(card6);
        testPlayer1Score.put(player1, cardsPlayer1);
        testPlayer2Score.put(player2, cardsPlayer2);
        Assertions.assertEquals(78, game.calculateScore(testPlayer1Score.get(player1)));
        Assertions.assertEquals(109, game.calculateScore(testPlayer2Score.get(player2)));
    }

    @Test
    void testWhoStartsFirst() {
        int start1 = game.getCurrentPlayerIndex();
        int start2 = game.getCurrentPlayerIndex();
        int start3 = game.getCurrentPlayerIndex();

        boolean trueAnswer1 = false;
        boolean trueAnswer2 = false;
        boolean trueAnswer3 = false;

        if (start1 == 0 || start1 == 1) {
            trueAnswer1 = true;
        }
        if (start2 != 0 && start2 != 1) {
            trueAnswer2 = true;
        }
        if (start3 >= 0 && start3 <= 1) {
            trueAnswer3 = true;
        }
        Assertions.assertTrue(trueAnswer1);
        Assertions.assertFalse(trueAnswer2);
        Assertions.assertTrue(trueAnswer3);

    }

    @Test
    void testReturnValue() {
        Card card1 = new Card(Card.Color.Blue, Card.Value.Three);
        Card card2 = new Card(Card.Color.Blue, Card.Value.Eight);
        Card card3 = new Card(Card.Color.Red, Card.Value.Nine);

        int value1 = game.returnValue(card1);
        int value2 = game.returnValue(card2);
        int value3 = game.returnValue(card3);

        Assertions.assertEquals(3, value1);
        Assertions.assertEquals(8, value2);
        Assertions.assertEquals(9, value3);
    }

    @Test
    void testTypeOfCard() {
        Card card1 = new Card(Card.Color.Blue, Card.Value.Three);
        Card card2 = new Card(Card.Color.Blue, Card.Value.DrawTwo);
        Card card3 = new Card(Card.Color.None, Card.Value.Wild);

        boolean test1 = game.isNumberCard(card1);
        boolean test2 = game.isActionCard(card2);
        boolean test3 = game.isWildCard(card3);

        Assertions.assertTrue(test1);
        Assertions.assertTrue(test2);
        Assertions.assertTrue(test3);
    }

    @Test
    public void testStartNewRound() {
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player("p1"));
        players.add(new Player("p2"));
        Game game = new Game(players);
        game.startNewRound();
        Assertions.assertEquals(93, game.getDeck().getSizeDeck());
        Assertions.assertEquals(7, game.playerHands.get(players.get(0)).size());
        Assertions.assertEquals(0, game.playerScores.get(players.get(0)).intValue(), "Player scores should be reset to 0");
    }
}