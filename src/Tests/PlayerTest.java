package Tests;

import UNO.Model.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void testPlayerName() {
        Player player1 = new Player("Iosif");
        Player player2 = new Player("Victor");
        assertEquals("Iosif", player1.getName());
        assertEquals("Victor", player2.getName());
    }
}