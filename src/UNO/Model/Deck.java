package UNO.Model;

import java.util.ArrayList;
import java.util.Collections;

public class Deck {

    private final ArrayList<Card> deck;
    private int currentCard;
    private Game game;

    public Deck() {
        this.deck = new ArrayList<>();
    }

    /**
     * Initializes the deck with all the UNO cards
     **/
    public void init() {
        currentCard = 0;
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 4; ++j) {
                if (i == 0) {
                    deck.add(currentCard++, new Card(Card.colors[j], Card.values[i]));
                    continue;
                }
                deck.add(currentCard++, new Card(Card.colors[j], Card.values[i]));
                deck.add(currentCard++, new Card(Card.colors[j], Card.values[i]));
            }
        }
        for (int i = 10; i < 13; ++i) {
            for (int j = 0; j < 4; ++j) {
                deck.add(currentCard++, new Card(Card.colors[j], Card.values[i]));
                deck.add(currentCard++, new Card(Card.colors[j], Card.values[i]));
            }
        }
        for (int i = 0; i < 4; ++i) {
            deck.add(currentCard++, new Card(Card.colors[4], Card.values[13]));
            deck.add(currentCard++, new Card(Card.colors[4], Card.values[14]));
        }
    }

    /**
     * @param index the position of a card from the deck
     * @return the card from the deck situated at the position 'index'
     * @ensures index > 0 && index < deck.size()-1
     **/
    public Card getCard(int index) {
        return deck.get(index);
    }

    /**
     * Gives 1 card to the player
     *
     * @return the last card from the deck
     **/
    public Card drawCard() {
        if (deck.isEmpty()) {
            if (game.getCardsDown().isEmpty()) {
                init();
                shuffleDeck();
            } else {
                Card lastCard = game.getCardsDown().get(game.getCardsDown().size() - 1);
                game.getCardsDown().remove(lastCard);
                Collections.shuffle(game.getCardsDown());
                replaceCardsInDeck(game.getCardsDown());
                game.getCardsDown().clear();
                game.getCardsDown().add(lastCard);
            }
        }
        Card cardToDraw = deck.get(deck.size() - 1);
        deck.remove(cardToDraw);
        return cardToDraw;
    }

    /**
     * Used only at the beginning of the round when the game is not yet initialized
     *
     * @return the last card from the deck
     **/
    public Card drawFirstCard() {
        deck.remove(deck.get(deck.size() - 1));
        return deck.get(deck.size() - 1);
    }

    /**
     * Gives n cards to the player
     *
     * @param n the amount of cards that will be given to the player
     * @return returns an ArrayList of Cards for the player
     **/
    public ArrayList<Card> drawMultipleCards(int n) {
        if (n > deck.size()) {
            throw new IllegalArgumentException("There are only " + deck.size() + " left in the deck.");
        }
        ArrayList<Card> cardsToDraw = new ArrayList<>();
        for (int i = 0; i < n; ++i) {
            cardsToDraw.add(deck.get(deck.size() - 1));
            deck.remove(deck.get(deck.size() - 1));
        }
        return cardsToDraw;
    }

    /**
     * Shuffles the cards from the deck
     **/
    public void shuffleDeck() {
        Collections.shuffle(deck);
    }

    /**
     * @return the deck size
     **/
    public int getSizeDeck() {
        return deck.size();
    }

    /**
     * Add cards in the deck (used when the deck has no cards left, then the cards that have been put down go back in the deck)
     **/
    public void replaceCardsInDeck(ArrayList<Card> newCards) {
        deck.addAll(newCards);
    }

    /**
     * Removes all the cards from the deck
     **/
    public void empty() {
        deck.clear();
    }

    @Override
    public String toString() {
        return "Deck{" +
                "deck=" + deck +
                ", currentCard=" + currentCard +
                '}';
    }
}
