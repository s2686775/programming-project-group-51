package UNO.Model;

public class Card {
    private final Color color;
    private final Value value;
    public static final Color[] colors = Color.values();
    public static final Value[] values = Value.values();

    public enum Color {
        Red, Blue, Green, Yellow, None
    }

    public enum Value {
        Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine, DrawTwo, Reverse, Skip, Wild, WildFour
    }

    public Card(Color color, Value value) {
        this.color = color;
        this.value = value;
    }

    /**
     * @return the color of the card
     */
    public Color getColor() {
        return color;
    }

    /**
     * @return the value of the card
     */
    public Value getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Card{" +
                "color=" + color +
                ", value=" + value +
                '}' + '\n';
    }
}
