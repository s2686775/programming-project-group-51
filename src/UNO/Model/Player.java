package UNO.Model;

public class Player {
    protected String name;

    public Player(String name) {
        this.name = name;
    }

    /**
     * @return the name of the player
     */
    public String getName() {
        return name;
    }
}
