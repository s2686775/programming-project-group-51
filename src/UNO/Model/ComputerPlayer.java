package UNO.Model;

import java.util.*;

public class ComputerPlayer extends Player {

    private String color;
    private final List<String> colors = new ArrayList<>();

    public ComputerPlayer(String name) {
        super(name);
        colors.add("blue");
        colors.add("yellow");
        colors.add("red");
        colors.add("green");
    }

    /**
     * Returns the index of a playable card, starting from 1 or returns -1 if no card can be played by the computer
     *
     * @param game the current game of UNO
     * @param cards the cards that are in the hand of the computer player
     * @return the index of the playable card (from the computer player's hand) or -1 if there is not a single valid card
     */
    public int indexPlayableCard(Game game, ArrayList<Card> cards) {
        color = null;
        boolean playWild = false;
        int indexWild = 0;

        for (int i = 0; i < cards.size(); ++i) {
            if (cards.get(i).getValue().equals(Card.Value.Wild) || cards.get(i).getValue().equals(Card.Value.WildFour)) {
                playWild = true;
                indexWild = i + 1;
            } else if (game.isValid(game.getCardsDown().get(game.getCardsDown().size() - 1), cards.get(i)))
                return i + 1;
        }
        if (playWild) {
            color = colors.get(randomColor());
            return indexWild;
        }
        return -1;
    }

    /**
     * @return the color of a card
     */
    public String getColor() {
        return color;
    }

    /**
     * @return a random color used for when the computer player places a WILD card
     */
    public int randomColor() {
        Random random = new Random();
        return random.nextInt(colors.size());
    }

}