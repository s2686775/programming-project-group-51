package UNO.Model;

public class Commands {
    public final static String HELLO = "HELLO";
    public final static String READY = "READY";
    public final static String MOVE = "MOVE";
    public final static String DRAW = "DRAW";
    public final static String QUIT = "QUIT";
    public final static String SEPARATOR = " ";
    public final static String END = "END";
    public final static String GAMEOVER = "GAMEOVER";
    public final static String ERROR = "ERROR";
    public final static String CHAT = "CHAT";
}
