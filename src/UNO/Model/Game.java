package UNO.Model;

import java.util.*;

public class Game {

    public Map<Player, ArrayList<Card>> playerHands = new HashMap<>();
    public Map<Player, Integer> playerScores = new HashMap<>();
    private Card.Color currentColor; // the current color of the last card that is down
    private boolean directionOfPlay;
    private ArrayList<Player> playerList;
    private Deck deck;
    private ArrayList<Card> cardsDown = new ArrayList<>(); // the list of cards that are put down by the players
    private int currentPlayerIndex = 0;

    public Game(ArrayList<Player> players) {
        this.playerList = players;
        playerScores.put(playerList.get(0), 0);
        playerScores.put(playerList.get(1), 0);
        startNewRound();
    }

    /**
     * This method starts a new round of the game
     * It initializes a new deck and shuffles it
     * Then selects the first player and gives each player 7 starting cards
     * The first card is drawn and placed on the table
     * If the first card is a WildFour, the deck is shuffled again until it is not a WildFour
     */
    public void startNewRound() {
        System.out.println("------------------NEW ROUND------------------");
        deck = new Deck();
        deck.init();
        deck.shuffleDeck();
        whoStartsFirst(deck);
        System.out.println("The player who is starting the game is: " + playerList.get(currentPlayerIndex).getName());
        playerHands.put(playerList.get(0), new ArrayList<>());
        playerHands.put(playerList.get(1), new ArrayList<>());
        giveStartingCards(playerHands);
        System.out.println("These are the cards for " + playerList.get(0).getName() + '\n' + playerHands.get(playerList.get(0)));
        System.out.println("These are the cards for " + playerList.get(1).getName() + '\n' + playerHands.get(playerList.get(1)));
        cardsDown = new ArrayList<>();
        Card firstCard = deck.drawFirstCard();
        while (firstCard.getValue().equals(Card.Value.WildFour)) {
            deck.empty();
            deck.init();
            deck.shuffleDeck();
            firstCard = deck.drawFirstCard();
        }
        cardsDown.add(firstCard);
        System.out.println("The first card is: " + firstCard);
    }

    /**
     * Gives 7 cards to each player at the beginning of the round
     *
     * @param playerFirstCards a map of players and their corresponding initial cards
     */
    public void giveStartingCards(Map<Player, ArrayList<Card>> playerFirstCards) {
        for (Map.Entry<Player, ArrayList<Card>> entry : playerFirstCards.entrySet()) {
            playerHands.put(entry.getKey(), deck.drawMultipleCards(7));
        }
    }

    /**
     * Places a card from a player's hand
     *
     * @param player the player who is placing the card
     * @param card the card that the player wants to place
     */
    public void placeCard(Player player, Card card) {
        if (!playerHands.get(player).contains(card))
            System.out.println("You do not have this card in your hand.");
        if (isValid(cardsDown.get(cardsDown.size() - 1), card)) {
            cardsDown.add(card);
            playerHands.get(player).remove(card);
            if (!card.getValue().equals(Card.Value.Wild) && !card.getValue().equals(Card.Value.WildFour))
                this.currentColor = card.getColor();
        } else {
            System.out.println("It is not possible to place this card.");
        }
    }

    /**
     * A card is being drawn randomly from the deck for each player, whatever player has the card with the bigger
     * value, starts the round
     *
     * Decides which player starts the game, if it's 0 is the first player if it's 1 is the second player
     */
    public void whoStartsFirst(Deck deck) {
        int randomNumber0 = (int) (Math.random() * (108));
        Card randomCard0 = deck.getCard(randomNumber0);
        while (!isNumberCard(randomCard0)) {
            randomNumber0 = (int) (Math.random() * (108));
            randomCard0 = deck.getCard(randomNumber0);
        }
        int randomNumber1 = (int) (Math.random() * (108));
        Card randomCard1 = deck.getCard(randomNumber1);
        while (!isNumberCard(randomCard1)) {
            randomNumber1 = (int) (Math.random() * (108));
            randomCard1 = deck.getCard(randomNumber1);
        }
        if (isNumberCard(randomCard0) && isNumberCard(randomCard1) && returnValue(randomCard1) >= returnValue(randomCard0)) {
            currentPlayerIndex = 1;
        }
    }

    /**
     * Returns the numeric value of the given card
     *
     * @param card the card whose value is to be returned
     * @return an integer representing the value of the card
     */
    public int returnValue(Card card) {
        if (card.getValue().equals(Card.Value.Zero))
            return 0;
        else if (card.getValue().equals(Card.Value.One))
            return 1;
        else if (card.getValue().equals(Card.Value.Two))
            return 2;
        else if (card.getValue().equals(Card.Value.Three))
            return 3;
        else if (card.getValue().equals(Card.Value.Four))
            return 4;
        else if (card.getValue().equals(Card.Value.Five))
            return 5;
        else if (card.getValue().equals(Card.Value.Six))
            return 6;
        else if (card.getValue().equals(Card.Value.Seven))
            return 7;
        else if (card.getValue().equals(Card.Value.Eight))
            return 8;
        else if (card.getValue().equals(Card.Value.Nine))
            return 9;
        return -1;
    }

    /**
     * Checks if a card is an action card
     * An action card is defined as having the value of Reverse, Skip, DrawTwo
     *
     * @param card the card to checked
     * @return a boolean indicating if the card is an action card.
     */
    public boolean isActionCard(Card card) {
        if (card.getValue().equals(Card.Value.Reverse))
            return true;
        else if (card.getValue().equals(Card.Value.Skip))
            return true;
        else return card.getValue().equals(Card.Value.DrawTwo);
    }

    /**
     * Determines if a card is a wild card
     * A wild card is defined as having the value of Wild or WildFour
     *
     * @param card the card to be checked
     * @return True if the card is a wild card, False otherwise
     */
    public boolean isWildCard(Card card) {
        if (card.getValue().equals(Card.Value.Wild))
            return true;
        else return card.getValue().equals(Card.Value.WildFour);
    }

    /**
     * Returns whether the given card is a number card
     * A number card is defined as having the value of Zero, One, Two, Three, Four, Five, Six, Seven, Eight, or Nine
     *
     * @param card The card to be checked
     * @return True if the card is a number card, False otherwise
     */
    public boolean isNumberCard(Card card) {
        if (card.getValue().equals(Card.Value.Zero))
            return true;
        else if (card.getValue().equals(Card.Value.One))
            return true;
        else if (card.getValue().equals(Card.Value.Two))
            return true;
        else if (card.getValue().equals(Card.Value.Three))
            return true;
        else if (card.getValue().equals(Card.Value.Four))
            return true;
        else if (card.getValue().equals(Card.Value.Five))
            return true;
        else if (card.getValue().equals(Card.Value.Six))
            return true;
        else if (card.getValue().equals(Card.Value.Seven))
            return true;
        else if (card.getValue().equals(Card.Value.Eight))
            return true;
        else return card.getValue().equals(Card.Value.Nine);
    }

    /**
     * Determines whether a card is valid to be played on top of the current card
     *
     * If the current card is a number card, the card must be of the same color or the same value;
     * If the current card is an action card, the card must be of the same color or be a wild card;
     * If the current card is a wild card, the card must either be a wild card with a specified color,
     * or have the same color as the current color of the game
     *
     * @param currentCard The card that is currently on top of the cards that have been played (cardsDown)
     * @param card The card to be played
     * @return True if the card is valid to be played, False otherwise
     */
    public boolean isValid(Card currentCard, Card card) {
        if (isNumberCard(currentCard) && isNumberCard(card)) {
            return card.getColor().equals(currentCard.getColor()) || card.getValue().equals(currentCard.getValue());
        } else if (isNumberCard(currentCard) && !isNumberCard(card)) {
            if (card.getColor().equals(Card.Color.None))
                return true;
            else return card.getColor().equals(currentCard.getColor());
        } else if (!isNumberCard(currentCard) && !isNumberCard(card)) {
            if (card.getColor().equals(Card.Color.None))
                return true;
            if (this.isWildCard(currentCard) && !card.getColor().equals(Card.Color.None))
                if (card.getColor().equals(this.getCurrentColor()))
                    return true;
            return card.getValue().equals(currentCard.getValue()) || card.getColor().equals(currentCard.getColor());
        } else {
            if (isWildCard(currentCard) && card.getColor().equals(this.getCurrentColor()))
                return true;
            else return currentCard.getColor().equals(card.getColor());
        }
    }

    /**
     * Calculates the score of a hand of cards
     *
     * The score is calculated by adding up the values of each card that remained in the hand of the opponents
     * The value of a number card is its face value. The value of an action card is 20
     * The value of a wild card is 50
     *
     * @param cards The hand of cards to calculate the score for
     * @return The total score of the hand
     */
    public int calculateScore(ArrayList<Card> cards) {
        int score = 0;
        for (Card card : cards)
            if (isNumberCard(card))
                score += returnValue(card);
            else if (isActionCard(card))
                score += 20;
            else score += 50;

        return score;
    }

    /**
     * Sets the score of a player
     *
     * @param player The player whose score is to be set
     * @param score The new score for the player
     */
    public void setScore(Player player, int score) {
        playerScores.put(player, score);
    }

    /**
     * Checks if the round is over (when one player remains without any cards in the hand)
     */
    public boolean isRoundOver() {
        for (Player player : playerList) {
            if (playerHands.get(player).isEmpty())
                return true;
        }
        return false;
    }

    /**
     * Checks if the game is over (when one player has reached a score of 500 or bigger)
     */
    public boolean isGameOver() {
        for (Player player : playerList) {
            if (playerScores.get(player) >= 500)
                return true;
        }
        return false;
    }

    /**
     * Will display in the console the scores of each player as well as their names
     */
    public void showScores() {
        for (Map.Entry<Player, Integer> entry : playerScores.entrySet()) {
            System.out.println("Player: " + entry.getKey().getName() + " has " + entry.getValue() + " points.");
        }
    }

    /**
     * Changes the direction of play
     */
    public void changeDirectionOfPlay() {
        directionOfPlay = !directionOfPlay;
    }

    /**
     * When a wild card is played, the player will have the option to choose the color of the next card
     * The variable currentColor will be changed to the new color
     */
    public void changeCurrentColor(String color) {
        switch (color) {
            case "blue" -> currentColor = Card.Color.Blue;
            case "red" -> currentColor = Card.Color.Red;
            case "yellow" -> currentColor = Card.Color.Yellow;
            case "green" -> currentColor = Card.Color.Green;
            default -> throw new IllegalArgumentException("You cannot choose this color.");
        }
    }

    /**
     * @return the current player
     */
    public Player getCurrentPlayer() {
        return playerList.get(currentPlayerIndex);
    }

    /**
     * Returns the hand of a player
     *
     * @param player The player whose hand is to be returned
     * @return The hand of the player as a list of cards
     */
    public ArrayList<Card> getPlayerHand(Player player) {
        return playerHands.get(player);
    }

    /**
     * @return the cards that have been played by the players (the cards that are on the table)
     */
    public ArrayList<Card> getCardsDown() {
        return cardsDown;
    }

    /**
     * @return the deck which consists of the cards that have not been played yet (the draw pile)
     */
    public Deck getDeck() {
        return deck;
    }

    /**
     * Checks if the turn is over by checking if the player has no cards in their hand
     *
     * @param player the method returns true if the hand of the given player is empty
     * and false if the player still has cards in its hand
     */
    public boolean isRoundOver(Player player) {
        return playerHands.get(player).isEmpty();
    }

    /**
     * @return the list of players
     */
    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    /**
     * @ensures the current player is updated after each move;
     */
    public void nextPlayer() {
        this.currentPlayerIndex = (this.currentPlayerIndex + 1) % playerList.size();
    }

    /**
     * @return the color of the last card that was played by any player
     */
    public Card.Color getCurrentColor() {
        return currentColor;
    }

    /**
     * Switches the hands of the two players in the game.
     *
     * This method retrieves the hands of the first and second player in the playerList,
     * swaps them, and updates the playerHands map with the new hands.
     */
    public void switchHands() {
        ArrayList<Card> temp = playerHands.get(playerList.get(0));
        playerHands.put(playerList.get(0), playerHands.get(playerList.get(1)));
        playerHands.put(playerList.get(1), temp);
    }

    /**
     * Returns the score of a player.
     *
     * @param player The player whose score is to be returned
     * @return The score of the player
     */
    public Integer getPlayerScores(Player player) {
        return playerScores.get(player);
    }

    /**
     * @return the index of the current player (from the players list)
     */
    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }
}
