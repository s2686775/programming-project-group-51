package Local.Controller;

import UNO.Model.Game;
import UNO.Model.Player;
import Local.View.LocalTUI;

import java.util.ArrayList;

public class LocalUNO {
    public static String finishMessage = "";
    private final LocalTUI view;
    private final Game game;
    public Player player1;
    public Player player2;

    public LocalUNO() {
        ArrayList<Player> players = new ArrayList<>();
        player1 = new Player("Iosif");
        player2 = new Player("Victor");
        players.add(player1);
        players.add(player2);

        this.view = new LocalTUI(this);
        this.game = new Game(players);
    }

    public static void main(String[] args) {
        LocalTUI localTUI = new LocalTUI(new LocalUNO());
        localTUI.run();
        System.out.println(finishMessage);
    }

    /**
     * @return the game
     */
    public Game getGame() {
        return game;
    }
}
