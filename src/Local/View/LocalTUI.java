package Local.View;

import Local.Controller.LocalUNO;
import UNO.Model.Card;
import Network.Exceptions.GameException;
import UNO.Model.Player;

import java.util.*;

public class LocalTUI implements Runnable {
    private LocalUNO controller;

    public LocalTUI(LocalUNO controller) {
        this.controller = controller;
    }

    @Override
    public void run() {
        try {
            startUI();
        } catch (GameException e) {
            e.printStackTrace();
        }
    }

    /**
     * Plays the game of UNO locally, although the final version is implemented in the networking part
     *
     * @throws GameException
     */
    private void startUI() throws GameException {
        System.out.println("Let's play UNOOOO!");
        Scanner in = new Scanner(System.in);
        System.out.println(System.lineSeparator() + "> ");
        boolean isSevenOuno = false;

        while (!controller.getGame().isGameOver()) {
            while (in.hasNextLine() && !controller.getGame().isRoundOver()) {
                Player currentPlayer = controller.getGame().getCurrentPlayer();
                String input = in.nextLine();
                if (input == null || input.isBlank()) continue;
                String[] splitInput = input.split(" ");
                String command = splitInput[0];
                switch (command) {
                    case "help":
                        System.out.println("""
                                The following commands can be used:\s
                                - QUIT: You quit and the game is going to end!
                                - MOVE: You select the index (starting from 1) of the card that you want to place and the color, in case the card is of WILD type such as: MOVE 1blue or MOVE 3
                                - DRAW: You can draw a card from the deck!
                                - CHAT: You will send a message to the other player, make sure to type the message after the command.
                                """);
                        break;
                    case "quit":
                        if (controller.getGame().getCurrentPlayer().equals(controller.player1)) {
                            System.out.println("Your opponent " + controller.player1.getName() + " gave up. Congratulations " + controller.player2.getName() + ", you won!");
                        } else {
                            System.out.println("Your opponent " + controller.player2.getName() + " gave up. Congratulations " + controller.player1.getName() + ", you won!");
                        }
                        System.exit(0);
                        break;
                    case "place":
                        // place {introduce index of card that you want to play} ex: place 4 (the fifth card will be played)
                        // handle the exception when the player can't play the card
                        int indexOfCard = Integer.parseInt(splitInput[1]);
                        ArrayList<Card> cardToBePlayed = controller.getGame().getPlayerHand(currentPlayer);
                        System.out.println(currentPlayer.getName() + " has put down: " + cardToBePlayed.get(indexOfCard).toString());
                        if (controller.getGame().isWildCard(cardToBePlayed.get(indexOfCard))) {
                            if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.Wild)) {
                                // choose the color of the next card (get input from the player about the card)
                                if (splitInput[2].isEmpty())
                                    System.out.println("You did not pick the color");
                                else {
                                    String colorInputFromPlayer = splitInput[2];
                                    controller.getGame().changeCurrentColor(colorInputFromPlayer);
                                }
                            } else if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.WildFour)) {
                                // next player draws 4 cards and loses their turn
                                ArrayList<Card> cardsCurrPlayer = controller.getGame().getPlayerHand(currentPlayer);
                                for (Card card : cardsCurrPlayer) {
                                    if (card.getColor() == controller.getGame().getCurrentColor())
                                        System.out.println("You are not allowed to play this card because you have a card" +
                                                "in your hand that matches the color of the card that is currently down");
                                }
                                if (splitInput[2].isEmpty())
                                    System.out.println("You did not pick the color");
                                else {
                                    String colorInputFromPlayer = splitInput[2];
                                    controller.getGame().changeCurrentColor(colorInputFromPlayer);
                                    for (Player p : controller.getGame().getPlayerList()) {
                                        if (!currentPlayer.equals(p)) {
                                            controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                            controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                            controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                            controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                        }
                                    }
                                    controller.getGame().nextPlayer();
                                }
                            }
                        } else if (controller.getGame().isActionCard(cardToBePlayed.get(indexOfCard))) {
                            if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.DrawTwo) && controller.getGame().isValid(controller.getGame().getCardsDown().get(controller.getGame().getCardsDown().size() - 1), cardToBePlayed.get(indexOfCard))) {
                                // forward the 2 cards to the next player
                                for (Player p : controller.getGame().getPlayerList()) {
                                    if (!currentPlayer.equals(p)) {
                                        controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                        controller.getGame().getPlayerHand(p).add(controller.getGame().getDeck().drawCard());
                                    }
                                }
                                // next player draws 2 cards and loses their turn
                            } else if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.Reverse)) {
                                // reverse the direction of play
                                controller.getGame().changeDirectionOfPlay();
                            } else if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.Skip) && controller.getGame().isValid(controller.getGame().getCardsDown().get(controller.getGame().getCardsDown().size() - 1), cardToBePlayed.get(indexOfCard))) {
                                // next player loses their turn
                                controller.getGame().nextPlayer();
                                //controller.getGame().nextPlayer();
                            }
                        }//Seven-O UNO non-mandatory part
                        else if (controller.getGame().isNumberCard(cardToBePlayed.get(indexOfCard))) {
                            if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.Zero)) {
                                // every time this card is played all players pass their hands to the next player in direction of play
                                isSevenOuno = true;
                                controller.getGame().placeCard(currentPlayer, cardToBePlayed.get(indexOfCard));
                                controller.getGame().switchHands();
                            } else if (cardToBePlayed.get(indexOfCard).getValue().equals(Card.Value.Seven)) {
                                // the player who played this card MUST trade their hand with a player of their choice
                                isSevenOuno = true;
                                controller.getGame().placeCard(currentPlayer, cardToBePlayed.get(indexOfCard));
                                controller.getGame().switchHands();
                            }
                        }
                        if(!isSevenOuno) {
                            controller.getGame().placeCard(currentPlayer, cardToBePlayed.get(indexOfCard));
                            isSevenOuno = false;
                        }
                        System.out.println("The cards that are down are: " + controller.getGame().getCardsDown());
                        if (controller.getGame().isRoundOver(currentPlayer)) {
                            Player p0 = controller.getGame().getPlayerList().get(0);
                            Player p1 = controller.getGame().getPlayerList().get(1);
                            int scoreP0 = controller.getGame().calculateScore(controller.getGame().getPlayerHand(p1));
                            int scoreP1 = controller.getGame().calculateScore(controller.getGame().getPlayerHand(p0));
                            if (scoreP0 >= 500 || scoreP1 >= 500) {
                                //game over declare winner
                                String winner = p1.getName();
                                if(scoreP0 > scoreP1)
                                    winner = p0.getName();
                                controller.getGame().setScore(p0, scoreP0);
                                controller.getGame().setScore(p1, scoreP1);
                                controller.getGame().showScores();
                                System.out.println("The game is over! Congratulations, " + winner + " has won!");
                                System.exit(0);
                                break;
                            } else {
                                // round over
                                controller.getGame().setScore(p0, scoreP0);
                                controller.getGame().setScore(p1, scoreP1);
                                controller.getGame().showScores();
                                controller.getGame().startNewRound();
                            }
                        }
                        break;
                    case "draw":
                        // after typing draw, a card will be added to your hand and the round will be skipped
                        //  handle the action of a player drawing multiple cards (the method is already done drawMultipleCards() from class Deck)
                        //  - everything is handled for the network part
                        if (controller.getGame().getDeck().getSizeDeck() == 0) {
                            Collections.shuffle(controller.getGame().getCardsDown());
                            controller.getGame().getDeck().replaceCardsInDeck(controller.getGame().getCardsDown());
                            controller.getGame().getCardsDown().clear();
                        }
                        controller.getGame().getPlayerHand(currentPlayer).add(controller.getGame().getDeck().drawCard());
                        System.out.println("The cards that are down are: " + controller.getGame().getCardsDown());
                        break;
                    case "chat":
                        // a nice message will be sent to the other player
                        break;
                    default:
                        System.out.println("Invalid command.");
                        break;
                }
                controller.getGame().nextPlayer();
                currentPlayer = controller.getGame().getCurrentPlayer();
                notifyPlayer(currentPlayer, controller.getGame().getPlayerHand(currentPlayer));
                System.out.println(System.lineSeparator() + "> ");
            }
        }
    }

    /**
     * Displays the current player and its cards
     *
     * @param player the given player
     * @param playerCards the cards of the specific player
     */
    public void notifyPlayer(Player player, ArrayList<Card> playerCards) {
        System.out.println("Current player: " + player.getName() + ". \nCards: " + playerCards);
    }
}
